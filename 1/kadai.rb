#!/usr/bin/ruby -Ku
# -*- coding: utf-8 -*-

require 'date'

def disp(arr)
  puts"-----------------"
  label=["書籍名:","著者名:","出版社：","ページ数：","発刊日:"]
  label.size.times{|i|
    puts label[i].to_s+arr[i].to_s
  }
end

jissen=[
  "実践アジャイル　ソフトウェア開発法とプロジェクト管理",
  "山田　正樹",
  "ソフトリサーチセンター",
  248,
  Date.new(2005,1,25)
]
disp(jissen)

nyumon=[
  "入門LEGO　MINDSTORMS　NXT　レゴブロックで作る動くロボット",
  "大庭　慎一郎",
  "ソフトバンククリエイティブ",
  164,
  Date.new(2006,12,23)
]
disp(nyumon)





