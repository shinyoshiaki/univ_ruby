#! /usr/bin/ruby -Ku
# -*- coding: utf-8 -*-

require 'dbi'
require 'date'

$item_name = {'id' => "キー", 'title' => "書籍名", 'author' => "著者名", 'page' => "ページ数", 'publish_date' => "発刊日"}

class BookInfo
  def initialize(title, author, page, publish_date)
    @title = title
    @author = author
    @page = page
    @publish_date = publish_date
  end

  attr_accessor :title, :author, :page, :publish_date

  def to_s
    "#{@title}, #{@author}, #{@page}, #{@publish_date}"
  end

  def to_formatted_string(sep = "\n")
    "書籍名 : #{@title}#{sep}著者名 : #{@author}#{sep}ページ数 : #{page}ページ#{sep}発刊日 : #{@publish_date}#{sep}"
  end
end

class BookInfoManager
  def initialize(sqlite_name)
    @db_name = sqlite_name
    @dbh = DBI.connect("DBI:SQLite3:#{@db_name}")
  end

  def init_book_infos
    puts "\n0. 蔵書データベースの初期化"
    print "初期化しますか?(Y/yなら削除を実行します): "
    yesno = gets.chomp.upcase

    if /^Y$/ =~ yesno
      @dbh.do("drop table if exists bookinfos")
      @dbh.do("create table bookinfos (
        id           varchar(50)  not null,
        title        varchar(100) not null,
        author       varchar(100) not null,
        page         int          not null,
        publish_date datetime     not null,
        primary      key(id));")
      puts "\nデータベースを初期化しました。"
    end
  end

  def add_book_info
    puts "\n1. 蔵書データの登録"
    print "蔵書データを登録します。"

    book_info = BookInfo.new("", "", 0, Date.new)
    print "\nキー: "
    key = gets.chomp
    print "書籍名 : "
    book_info.title = gets.chomp
    print "著者名 : "
    book_info.author = gets.chomp
    print "ページ数 : "
    book_info.page = gets.chomp.to_i
    print "発刊年 : "
    year = gets.chomp.to_i
    print "発刊月 : "
    month = gets.chomp.to_i
    print "発刊日 : "
    day = gets.chomp.to_i
    book_info.publish_date = Date.new(year, month, day)

    @dbh.do("insert into bookinfos values (
      \'#{key}\',
      \'#{book_info.title}\',
      \'#{book_info.author}\',
      #{book_info.page},
      \'#{book_info.publish_date}\');")
    puts "\n登録しました。"
  end

  def list_all_book_infos    
    puts "\n2. 蔵書データの表示"
    print "蔵書データを表示します。"

    puts "\n----------"

    sth = @dbh.execute("select * from bookinfos")

    counts = 0
    sth.each do |row|
      row.each_with_name do |val, name|
        puts "#{$item_name[name]}: #{val.to_s}"
      end
      puts "----------"
      counts += 1
    end

    sth.finish

    puts "\n#{counts}件表示しました。"
  end

  def update_book_info
    print "\nキー: "
    key = gets.chomp
   
    item_label={'title' => "書籍名", 'author' => "著者名", 'page' => "ページ数"}    
    item_label.each do |k,value|
        print value
        @dbh.do("update bookinfos set #{k}='#{gets.chomp}' where id='#{key}';")        
    end
     
    print "発刊年 : "
    year = gets.chomp.to_i
    print "発刊月 : "
    month = gets.chomp.to_i
    print "発刊日 : "
    day = gets.chomp.to_i    
    if year!=0 && month!=0 && day!=0
      date=Date.new(year, month, day)
      @dbh.do("update bookinfos set publish_date='#{date}' where id='#{key}';")    
    end
  end

  def input_book_info    
    item_label={'id' => "キー", 'title' => "書籍名", 'author' => "著者名", 'page' => "ページ数"}    
    words={}

    item_label.each do |key,value|
        print value
        words[key]=gets.chomp.to_s
    end

    print "発刊年 : "
    year = gets.chomp.to_i
    print "発刊月 : "
    month = gets.chomp.to_i
    print "発刊日 : "
    day = gets.chomp.to_i    
    if year!=0 && month!=0 && day!=0
        date=Date.new(year, month, day)
        words["publish_date"]=date.to_s
    end

    return words
  end

  def search_book_info
    sth = @dbh.execute("select * from bookinfos")
        
    words=input_book_info

    puts "\n----------"
    puts "検索結果\n\n"

    sth.each do |row|
      bool=false
      row.each_with_name do |val, name|
        if words[name]==val.to_s
            bool=true
        end
      end
      if bool==true
        row.each_with_name do |val, name|
            puts "#{$item_name[name]}: #{val.to_s}"
        end
        puts "----------"
      end
    end

    sth.finish
  end

  def delete_book_info
    sth = @dbh.execute("select * from bookinfos")
    
    words=input_book_info

    sth.each do |row|
      bool=false
      id=""
      row.each_with_name do |val, name|
        if name=="id"
          id=val
        end
        if words[name]==val.to_s
            bool=true
        end
      end
      if bool==true
        puts "キー「#{id}」を削除します。"
        @dbh.do("delete from bookinfos where id='#{id}';")    
      end
    end
    sth.finish
  end

  def run
    while true
      puts ""
      puts "0. 蔵書データベースの初期化"
      puts "1. 蔵書データの登録"
      puts "2. 蔵書データを表示"
      puts "3. 蔵書データの更新"
      puts "4. 蔵書データの削除"
      puts "5. 蔵書データの検索"
      puts "9. 終了"
      puts "番号を選んでください(0, 1, 2, 3, 4, 5, 9)"

      case gets.chomp
      when '0'
        init_book_infos
      when '1'
        add_book_info
      when '2'
        list_all_book_infos
      when '3'
        update_book_info
      when '4'
        delete_book_info
      when '5'
        search_book_info
      when '9'
        @dbh.disconnect
        puts "\n終了しました。"
        break
      else
      end
    end
  end
end

book_info_manager = BookInfoManager.new("book_info_sqlite.db")
book_info_manager.run
