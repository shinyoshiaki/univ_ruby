require 'digest/sha2'

dif=5
target=""
dif.times do
    target+="0"
end

nonce=0
seed="seed"

while true
    nonce+=1
    check=seed+nonce.to_s    
    res=(Digest::SHA256.hexdigest check).slice(1,dif)    
    if res==target
        break
    end
end

p nonce
