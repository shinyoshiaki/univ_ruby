import java.security.MessageDigest;

class SHA2 {
        static int diff = 5;

        public static void main(String[] args) {
                String text = "seed";
                String target = "";
                for (int i = 0; i < diff; i++)
                        target += "0";

                int nonce;
                for (nonce = 0;; nonce++) {
                        String check = text + String.valueOf(nonce);
                        String res = calc(check).substring(0, diff);
                        if (res.equals(target))
                                break;
                }
                System.out.println(nonce);
        }

        static String calc(String text) {
                byte[] cipher_byte;
                try {
                        MessageDigest md = MessageDigest.getInstance("SHA-1");
                        md.update(text.getBytes());
                        cipher_byte = md.digest();
                        StringBuilder sb = new StringBuilder(2 * cipher_byte.length);
                        for (byte b : cipher_byte) {
                                sb.append(String.format("%02x", b & 0xff));
                        }
                        return sb.toString();
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return null;
        }
}