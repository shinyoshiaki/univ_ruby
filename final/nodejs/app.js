const sha256 = require("sha256")

const dif = 5
let target = ''
for (let i = 0; i < dif; i++)target += '0'

let nonce = 0
const seed = "seed"
for (nonce = 0; ; nonce++) {
    const check = seed + nonce.toString()
    const res = sha256(check).toString().substr(0, dif)
    if (res == target) break
}

console.log(nonce)
