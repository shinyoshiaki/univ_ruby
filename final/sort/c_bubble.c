#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10000

int sort[N];

void BubbleSort(int *data, int size)
{
    int i, j;
    int tmp = 0;
    for (i = 0; i < size - 1; i++)
        for (j = size - 1; j > i; j--)
            if (data[j - 1] > data[j])
            {
                tmp = data[j - 1];
                data[j - 1] = data[j];
                data[j] = tmp;
            }
}

int main(void)
{
    int i;

    for (i = 0; i < N; i++)
    {
        sort[i] = rand();
    }

    BubbleSort(sort, N);
}