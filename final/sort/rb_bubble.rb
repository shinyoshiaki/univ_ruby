def bsort(a)
  l = a.length
  for i in 1..l do
    for j in 1..(l-i) do
      if a[j - 1] > a[j]
        t = a[j]
        a[j] = a[j - 1]
        a[j - 1] = t
      end
    end
  end
  return a
end

N=10000

data=[]
N.times do
data.push(rand())
end

bsort(data)