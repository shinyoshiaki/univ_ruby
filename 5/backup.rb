#! /usr/bin/ruby -Ku
# -*- coding: utf-8 -*-

require "fileutils"

if ARGV[0]==nil
    puts "ファイルをバックアップします。\n./backup.rb ファイル名"
elsif File.exist?(ARGV[0])==false
    puts "#{ARGV[0]}が読めません。"
else    
    1.step do |i|
        if File.exist?("#{ARGV[0]}.#{i}")==false
            FileUtils.cp(ARGV[0],"#{ARGV[0]}.#{i}")
            puts "[#{ARGV[0]}] --> [#{ARGV[0]}.#{i}]"
            break
        end
    end
end