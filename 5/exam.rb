#! /usr/bin/ruby -Ku
# -*- coding: utf-8 -*-

list=[]
subjects=[]
open(ARGV[0],"r:UTF-8"){|file|
        subjects=file.gets.chomp.split(',')
        file.each_with_index do |line,i|            
            data=[]
            data=line.chomp.split(',')
            num=0
            data.each do|v|
                num=num+v.to_i
            end
            data.push(num)
            list.push(data)            
        end
        }

list.sort_by!{|x| x[x.length-1]}.reverse!

printf("氏名　　　")
subjects.each_with_index do |v,i|
    if i!=0
        printf("| #{v} ")
    end
end
puts"|合計"

def showLine(arr)
    printf ("----------")
    arr.each do 
        printf("+------")
    end
    puts ""
end

showLine(subjects)

list.each do |line|    
    line.each_with_index do |v,i|
        if i==0
            printf("%4s  ",v)
        else
            printf("|%4d  ",v)
        end
    end
    puts""
end

showLine(subjects)

rotated=list.transpose.map(&:reverse)
rotated.delete_at(0)

average=[]
hyojyun=[]
rotated.each do |line|    
    line.map!{|x| x.to_f}
    ave=line.inject(:+)/line.length
    average.push(ave)    
    
    hensa2=[]
    line.each do |v|
        hensa2.push((v.to_f-ave)**2)
    end
    sum=0
    hensa2.each do |v|
        sum+=v.to_f
    end
    hyojyun.push(Math.sqrt(sum/hensa2.length))
end

printf("平均　　　")
average.each do |v|
    printf("|%5.1f ",v)
end
puts ""
printf("標準偏差　")
hyojyun.each do |v|
    printf("|%5.1f ",v)
end
puts ""