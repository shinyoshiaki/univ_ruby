#! /usr/bin/ruby -Ku
# -*- coding: utf-8 -*-
require 'webrick'
#t18i917f
config={
    :Port=>50917,
    :DocumentRoot=>'.'
}

server=WEBrick::HTTPServer.new(config)

trap(:INT) do
    server.shutdown
end

server.start