# -*- coding: utf-8 -*-
require "webrick"
require "erb"
require "rubygems"
require "dbi"
require "date"

class String
  alias_method(:orig_concat, :concat)

  def concat(value)
    if RUBY_VERSION > "1.9"
      orig_concat value.force_encoding("UTF-8")
    else
      orig_concat value
    end
  end
end

config = {
  :Port => 50917,
  :DocumentRoot => ".",
}

WEBrick::HTTPServlet::FileHandler.add_handler("erb", WEBrick::HTTPServlet::ERBHandler)

server = WEBrick::HTTPServer.new(config)

server.config[:MimeTypes]["erb"] = "text/html"

server.mount_proc("/register") { |req, res|
  p req.query
  dbh = DBI.connect("DBI:SQLite3:boardsystem.db")
  rows = dbh.select_one("select * from users where id='#{req.query["id"]}';")
  if rows
    dbh.disconnect

    template = ERB.new(File.read("nonentried.erb"))
    res.body << template.result(binding)
  else
    dbh.do("insert into users values('#{req.query["id"]}','#{req.query["password"]}');")
    dbh.disconnect
    template = ERB.new(File.read("entried.erb"))
    res.body << template.result(binding)
  end
}

server.mount_proc("/board") { |req, res|
  dbh = DBI.connect("DBI:SQLite3:boardsystem.db")
  user_name = req.query["user_name"]
  dbh.do("insert into board values('#{user_name}','#{req.query["comment"]}','#{DateTime.now}');")
  dbh.disconnect
  send_id = user_name
  template = ERB.new(File.read("board.erb"))
  res.body << template.result(binding)
}

server.mount_proc("/login") { |req, res|
  p req.query
  dbh = DBI.connect("DBI:SQLite3:boardsystem.db")
  sth = dbh.execute("select * from users")

  user_name = ""

  exist = false
  sth.each do |row|
    if req.query["id"] == row["id"] && req.query["password"] == row["password"]
      user_name = req.query["id"]
      exist = true
      break
    end
  end

  sth.finish
  if exist
    send_id = user_name
    template = ERB.new(File.read("board.erb"))
    res.body << template.result(binding)
  else
    template = ERB.new(File.read("login_fail.erb"))
    res.body << template.result(binding)
  end
}

trap(:INT) do
  server.shutdown
end

server.start
